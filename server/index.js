const path = require('path');
const webpack = require('webpack');
const express = require('express');
var proxyMiddleware = require('http-proxy-middleware');

const config = require('./../webpack.config');

var app = express();
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use('/api', proxyMiddleware({
    target: 'http://expobook.dev',
    changeOrigin: true
}));

app.use('/upload', proxyMiddleware({
    target: 'http://expobook.dev',
    changeOrigin: true
}));

app.use('/fixtureFiles', proxyMiddleware({
    target: 'http://expobook.dev',
    changeOrigin: true
}));

app.get('*', function(req, res) {
    let imgExtensions = ['jpg', 'png', 'svg'];
    let ext = req.url.split('.').reverse()[0];

    if(imgExtensions.indexOf(ext) !== -1) {
        res.sendFile(req.url, {root: path.join(__dirname, '/..')});
        return;
    }

    res.sendFile(path.join(__dirname, '/../index.html'));
});

app.listen(3000, function(err) {
    if (err) {
        return console.error(err);
    }

    console.log('Listening at http://localhost:3000/');
});