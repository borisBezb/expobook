const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
    devtool: 'cheap-module-eval-source-events',
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: "/public"
    },
    module: {
        rules:[
            {
                test: /\.js$/,
                use: [
                    'react-hot-loader',
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ["es2015", { modules: false }],
                                "stage-2",
                                "react"
                            ],
                            plugins: [
                                "transform-decorators-legacy"
                            ]
                        }
                    }
                ],
                include: path.join(__dirname, 'src'),
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 1
                        }
                    },
                    "postcss-loader" // has separate config nearby
                ]
            },
            {
                test: /\.scss$/,
                /**use:
                    ExtractTextPlugin.extract({

                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                modules: true,
                                sourceMap: true,
                                importLoaders: 1
                            }
                        },
                        "sass-loader"
                    ]
                })*/
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: false
                        }
                    },
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        }),
        /**new HtmlWebpackPlugin({
            template: './build/index.html',
            filename: 'index.html',
        })*/
    ],
    resolve: {
        modules: [path.resolve(__dirname, "src"), "node_modules"]
    }
};