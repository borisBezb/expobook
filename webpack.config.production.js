const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: './src/index',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.[hash].js',
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new HtmlWebpackPlugin({
      template: './build/index.html',
      filename: 'index.html'
    }),
    new ExtractTextPlugin({ filename: 'index.[name].css', disable: false, allChunks: true })
  ],
  module: {
    rules: [
      {
          test: /\.js$/,
          use: [
              {
                  loader: 'babel-loader',
                  options: {
                      presets: [
                          ["es2015", { modules: false }],
                          "stage-2",
                          "react"
                      ],
                      plugins: [
                          "transform-decorators-legacy"
                      ]
                  }
              }
          ],
          include: path.join(__dirname, 'src'),
          exclude: /node_modules/
      },
      {
          test: /\.css$/,

          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use:
                [
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 1
                        }
                    },
                    "postcss-loader"
                ]
            })
      },
      {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
                  use: [
                      {
                          loader: "css-loader",
                          options: {
                              modules: false
                          }
                      },
                      "sass-loader"
                  ]
              })
      }
    ]
  },
    resolve: {
        modules: [path.resolve(__dirname, "src"), "node_modules"]
    }
};