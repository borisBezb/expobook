import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { events, event, stand, bookForm } from './modules/events/reducer';
import app from './modules/app/reducer';

export default combineReducers({
    events,
    event,
    stand,
    app,
    bookForm,
    routing: routerReducer
});