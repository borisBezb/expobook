import axios from 'axios/lib/axios'
import httpAdapter from 'axios/lib/adapters/http'

class ApiManager
{
    constructor(defaults) {
        this.options = defaults;
    }

    get(url) {
        return axios.get(url, this.options)
    }

    post(url, data) {
        return axios.post(url, data, this.options)
    }
}

const options = {
    baseURL: '/api/v1',
    timeout: 1000
}
if (process.env.NODE_ENV === 'test') {
    options.baseURL = 'http://localhost' + options.baseURL;
    options.adapter = httpAdapter;
}

const Api = new ApiManager(options);

export default Api;