import { createStore, compose, applyMiddleware } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
import initialState from './initialState';
import reducer from './reducer';
import DevTools from './containers/DevTools';

const store = createStore(
    reducer,
    initialState,
    compose(
        applyMiddleware(thunkMiddleware),
        applyMiddleware(routerMiddleware(browserHistory)),
        DevTools.instrument()
    )
);

export default store;