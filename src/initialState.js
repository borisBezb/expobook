export default {
    events: {
        isFetching: false,
        selectedEvent: 0,
        items: [],
    },
    event: {
        isFetching: false,
        detail: {},
    },
    stand: {
        isFetching: null,
        isBookFetching: false,
        detail: {}
    },
    bookForm: {
        submitted: false,
        model: {
            name: '',
            admin: '',
            email: '',
            contacts: '',
            logo: ''
        },
        errors: {
            name: [],
            admin: [],
            email: [],
            contacts: [],
            logo: []
        }
    },
    app: {
        message: '',
        state: ''
    }
};