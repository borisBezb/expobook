import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import GoogleMaps from 'google-maps-api';
import store from 'store';
import EventMarker from 'modules/events/containers/EventMarker';

const MapLoader = new GoogleMaps('AIzaSyDsT1_XpIn9K8v1h49SP2dv5J9Ym4htsKY');

export default MapLoader().then((maps) => {
    class MarkerOverlay extends maps.OverlayView
    {
        constructor(map) {
            super();

            this.setMap(map);
            this.markers = [];
        }

        addMarker(item) {
            this.markers.push(item);
        }

        draw() {
            if(0 == this.markers.length) {
                return;
            }
            console.log('draw')
            const markers = this.markers.map(
                (item, i) => (<EventMarker
                    event={ item }
                    maps={ maps }
                    mapOverlay={ this }
                    key={i}
                />)
            );

            ReactDOM.render(
                <Provider store={store}>
                    <div>
                        { markers }
                    </div>
                </Provider>,
                this.container
            );
        }

        addMapListener(event, callback) {
            this.map.addListener(event, e => callback());
        }

        onRemove() {
            ReactDOM.unmountComponentAtNode(this.container);
            this.markers = [];
        }

        onAdd() {
            const rootDiv = document.createElement('div');
            const panes = this.getPanes();
            panes.overlayImage.appendChild(rootDiv);

            this.container = rootDiv;
            this.draw();
        }
    }

    return {
        MarkerOverlay,
        maps
    };
});