import { event, stand, events } from '../reducer';
import initialState from 'initialState'
import * as eventActions from '../actions/event'
import * as eventsActions from '../actions/events'
import * as standActions from '../actions/stand'

describe('event reducer', () => {
    it('should return the initial state', () => {
        expect(
            event(undefined, {})
        ).toEqual(initialState.event)
    })

    it('should handle REQUEST_EVENT', () => {
        expect(
            event(undefined, eventActions.requestEvent())
        ).toEqual(
            {
                isFetching: true,
                detail: {},
            }
        )
    })

    it('should handle FETCH_EVENT', () => {
        expect(
            event({
                isFetching: true,
                detail: {},
            }, { type: eventActions.FETCH_EVENT, detail: {
                id:1,
                name:"Computer Gaming World",
                location:"Siberia Gaming Hall",
                latitude:55.011291959,
                longitude:82.9471206665,
                startDate:"2017-03-21 09:44:37",
                endDate:"2017-03-23 09:44:37",
                stands:[
                    {
                        id:114,
                        price:320,
                        currency:"USD",
                        company:null,
                        offsetX:20,
                        offsetY:20,
                        width:120,
                        height:40
                    }
                ]}
            })
        ).toEqual(
            {
                isFetching: false,
                detail: {
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                    stands:[
                        {
                            id:114,
                            price:320,
                            currency:"USD",
                            company:null,
                            offsetX:20,
                            offsetY:20,
                            width:120,
                            height:40
                        }
                    ]
                }
            }
        )
    })
})


describe('events reducer', () => {
    it('should return the initial state', () => {
        expect(
            events(undefined, {})
        ).toEqual(initialState.events)
    })

    it('should handle REQUEST_EVENTS', () => {
        expect(
            events(undefined, eventActions.requestEvent())
        ).toEqual(
            {
                isFetching: true,
                selectedEvent: 0,
                items: [],
            }
        )
    })

    it('should handle FETCH_EVENTS', () => {
        expect(
            events({
                isFetching: true,
                selectedEvent: 0,
                items: [],
            }, {
                type: eventsActions.FETCH_EVENTS, items: [{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                }]
            })
        ).toEqual(
            {
                isFetching: false,
                selectedEvent: 0,
                items: [{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                }]
            }
        )
    })

    it('should handle SELECT_EVENT', () => {
        expect(
            events({
                isFetching: false,
                selectedEvent: 0,
                items: [],
            }, {
                type: eventsActions.SELECT_EVENT,
                id: 1
            })
        ).toEqual(
            {
                isFetching: false,
                selectedEvent: 1,
                items: []
            }
        )
    })


    it('should handle UNSELECT_EVENT', () => {
        expect(
            events({
                isFetching: false,
                selectedEvent: 1,
                items: [],
            }, {
                type: eventsActions.UNSELECT_EVENT
            })
        ).toEqual(
            {
                isFetching: false,
                selectedEvent: 0,
                items: []
            }
        )
    })
})

describe('stand reducer', () => {
    it('should return the initial state', () => {
        expect(
            stand(undefined, {})
        ).toEqual(initialState.stand)
    })

    it('should handle REQUEST_STAND', () => {
        expect(
            stand(undefined, standActions.requestStand())
        ).toEqual(
            {
                isFetching: true,
                isBookFetching: false,
                detail: {}
            }
        )
    })

    it('should handle FETCH_STAND', () => {
        expect(
            stand({
                isFetching: true,
                isBookFetching: false,
                detail: {}
            }, {
                type: standActions.FETCH_STAND, detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            })
        ).toEqual(
            {
                isFetching: false,
                isBookFetching: false,
                detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            }
        )
    })

    it('should handle REQUEST_BOOKING', () => {
        expect(
            stand({
                isFetching: false,
                isBookFetching: false,
                detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            }, {
                type: standActions.REQUEST_BOOKING
            })
        ).toEqual(
            {
                isFetching: false,
                isBookFetching: true,
                detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            }
        )
    })


    it('should handle FETCH_BOOKING', () => {
        expect(
            stand({
                isFetching: false,
                isBookFetching: true,
                detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            }, {
                type: standActions.FETCH_BOOKING
            })
        ).toEqual(
            {
                isFetching: false,
                isBookFetching: false,
                detail: {
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            }
        )
    })
})