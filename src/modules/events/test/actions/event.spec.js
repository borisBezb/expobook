import * as actions from '../../actions/event'
import initialState from 'initialState';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import nock from 'nock'

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('event actions', () => {
    afterEach(() => {
        nock.cleanAll()
    })

    it('should create an action to set that event has been requested', () => {
        const expectedAction = {
            type: actions.REQUEST_EVENT
        };

        expect(actions.requestEvent()).toEqual(expectedAction)
    });

    it('should get an event', () => {
        const eventId = 1;

        nock('http://localhost')
            .get('/api/v1/event/' + eventId)
            .reply(200, {
                "code":200,
                "response":{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                    stands:[
                        {
                            id:114,
                            price:320,
                            currency:"USD",
                            company:null,
                            offsetX:20,
                            offsetY:20,
                            width:120,
                            height:40
                        }
                    ]
                }
            });

        const expectedActions = [
            { type: actions.REQUEST_EVENT },
            { type: actions.FETCH_EVENT, detail: {
                id:1,
                name:"Computer Gaming World",
                location:"Siberia Gaming Hall",
                latitude:55.011291959,
                longitude:82.9471206665,
                startDate:"2017-03-21 09:44:37",
                endDate:"2017-03-23 09:44:37",
                stands:[
                    {
                        id:114,
                        price:320,
                        currency:"USD",
                        company:null,
                        offsetX:20,
                        offsetY:20,
                        width:120,
                        height:40
                    }
                ]}
            }
        ];

        const store = mockStore(initialState.event);
        return store.dispatch(actions.fetchEvent(eventId))
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    })
})