import * as actions from '../../actions/events'
import initialState from 'initialState';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import nock from 'nock'

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('events actions', () => {
    afterEach(() => {
        nock.cleanAll()
    })

    it('should select some event', () => {
        const expectedAction = {
            type: actions.SELECT_EVENT,
            id: 1
        };

        expect(actions.selectEvent(1)).toEqual(expectedAction)
    });

    it('should unselect some event', () => {
        const expectedAction = {
            type: actions.UNSELECT_EVENT
        };

        expect(actions.unselectEvent()).toEqual(expectedAction)
    });

    it('should create an action to set that events has been requested', () => {
        const expectedAction = {
            type: actions.REQUEST_EVENTS
        };

        expect(actions.requestEvents()).toEqual(expectedAction)
    });

    it('should get an events', () => {
        nock('http://localhost')
            .get('/api/v1/event')
            .reply(200, {
                "code":200,
                "response":[{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                }]
            });

        const expectedActions = [
            { type: actions.REQUEST_EVENTS },
            { type: actions.FETCH_EVENTS, items: [{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                }]
            }
        ];

        const store = mockStore(initialState.events);
        return store.dispatch(actions.fetchEvents())
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    })
})