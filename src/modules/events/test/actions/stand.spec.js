import * as actions from '../../actions/stand'
import * as appActions from 'modules/app/actions';

import initialState from 'initialState';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import nock from 'nock'

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('stand actions', () => {
    afterEach(() => {
        nock.cleanAll()
    })

    it('should create an action to set that stand has been requested', () => {
        const expectedAction = {
            type: actions.REQUEST_STAND
        };

        expect(actions.requestStand()).toEqual(expectedAction)
    });

    it('should get an stand information', () => {
        const standId = 114;

        nock('http://localhost')
            .get('/api/v1/stand/' + standId)
            .reply(200, {
                "code":200,
                "response":{
                    id:114,
                    price:320,
                    currency:"USD",
                    company:null,
                    offsetX:20,
                    offsetY:20,
                    width:120,
                    height:40,
                    event:{
                        id:1,
                        name:"Computer Gaming World",
                        location:"Siberia Gaming Hall",
                        latitude:55.011291959,
                        longitude:82.9471206665,
                        startDate:"2017-03-21 09:44:37",
                        endDate:"2017-03-23 09:44:37",
                    }
                }
            });

        const expectedActions = [
            { type: actions.REQUEST_STAND },
            { type: actions.FETCH_STAND, detail: {
                id:114,
                price:320,
                currency:"USD",
                company:null,
                offsetX:20,
                offsetY:20,
                width:120,
                height:40,
                event:{
                    id:1,
                    name:"Computer Gaming World",
                    location:"Siberia Gaming Hall",
                    latitude:55.011291959,
                    longitude:82.9471206665,
                    startDate:"2017-03-21 09:44:37",
                    endDate:"2017-03-23 09:44:37",
                }
            }
            }
        ];

        const store = mockStore(initialState.stand);
        return store.dispatch(actions.fetchStand(standId))
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    })

    it('should create an action to set that booking has been requested', () => {
        const expectedAction = {
            type: actions.REQUEST_BOOKING
        };

        expect(actions.requestBooking()).toEqual(expectedAction)
    });

    it('should successfully book a place', () => {
        const stand = {
            id:114,
            price:320,
            currency:"USD",
            company:null,
            offsetX:20,
            offsetY:20,
            width:120,
            height:40,
            event:{
                id:1,
                name:"Computer Gaming World",
                location:"Siberia Gaming Hall",
                latitude:55.011291959,
                longitude:82.9471206665,
                startDate:"2017-03-21 09:44:37",
                endDate:"2017-03-23 09:44:37",
            }
        };

        const companyData = {
            name: 'Novosibirsk',
            admin: 'Boris',
            email: 'wc3inside@gmail.com',
            contacts: 'Novosibirsk, Lenina 1',
            logo: 15
        };

        nock('http://localhost')
            .post('/api/v1/stand/' + stand.id + '/book', companyData)
            .reply(201, {
                "code":201,
                "response":{
                    code:201,
                    response: {
                        id:21,
                        name:"Novosibirsk",
                        logo:"\/upload\/242\/28671288c5c91fbe5ea41f473faab59d.jpeg"
                    }
                }
            });

        const expectedActions = [
            { type: actions.REQUEST_BOOKING },
            { type: actions.FETCH_BOOKING },
            { type: appActions.APP_SET_MESSAGE, message: 'Place has been booked' },
            { payload: {args: ["/event/" + stand.event.id], method: "push"}, type: "@@router/CALL_HISTORY_METHOD"}
        ];

        const store = mockStore(initialState.stand);
        return store.dispatch(actions.fetchBooking(stand, companyData))
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    })

    it('should decline a try to book disabled place', () => {
        const stand = {
            id:114,
            price:320,
            currency:"USD",
            company:null,
            offsetX:20,
            offsetY:20,
            width:120,
            height:40,
            event:{
                id:1,
                name:"Computer Gaming World",
                location:"Siberia Gaming Hall",
                latitude:55.011291959,
                longitude:82.9471206665,
                startDate:"2017-03-21 09:44:37",
                endDate:"2017-03-23 09:44:37",
            }
        };

        const companyData = {
            name: 'Novosibirsk',
            admin: 'Boris',
            email: 'wc3inside@gmail.com',
            contacts: 'Novosibirsk, Lenina 1',
            logo: 15
        };

        nock('http://localhost')
            .post('/api/v1/stand/' + stand.id + '/book', companyData)
            .reply(400, {
                code:400,
                message:"Stand already has been booked by another company",
                params:{code:1}
            });

        const expectedActions = [
            { type: actions.REQUEST_BOOKING },
            { type: actions.FETCH_BOOKING },
            { type: appActions.APP_SET_MESSAGE, message: "Stand already has been booked by another company" },
            { payload: {args: ["/event/" + stand.event.id], method: "push"}, type: "@@router/CALL_HISTORY_METHOD"}
        ];

        const store = mockStore(initialState.stand);
        return store.dispatch(actions.fetchBooking(stand, companyData))
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    })
})