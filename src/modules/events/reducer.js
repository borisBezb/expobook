import * as actions from './actions/events';
import * as eActions from './actions/event';
import * as standActions from './actions/stand';
import initialState from 'initialState';
import baseForm from 'modules/forms/reducer';

export const events = (state = initialState.events, action) => {
    switch(action.type) {
        case actions.REQUEST_EVENTS:
            return {...state, isFetching: true};

        case actions.FETCH_EVENTS:
            return {
                ...state,
                isFetching: false,
                items: action.items
            };

        case actions.SELECT_EVENT:
            return {...state, selectedEvent: action.id};

        case actions.UNSELECT_EVENT:
            return {...state, selectedEvent: 0};

        default:
            return state;
    }
};

export const event = (state = initialState.event, action) => {
    switch(action.type) {
        case eActions.REQUEST_EVENT:
            return {...state, isFetching: true};

        case eActions.FETCH_EVENT:
            return {
                ...state,
                isFetching: false,
                detail: action.detail
            };

        default:
            return state;
    }
};

export const stand = (state = initialState.stand, action) => {
    switch(action.type) {
        case standActions.REQUEST_STAND:
            return {...state, isFetching: true};

        case standActions.REQUEST_BOOKING:
            return {...state, isBookFetching: true};

        case standActions.FETCH_BOOKING:
            return {...state, isBookFetching: false};

        case standActions.FETCH_STAND:
            return {
                ...state,
                isFetching: false,
                detail: action.detail
            };
        default:
            return state;
    }
};

export const bookForm = (state = initialState.bookForm, action) => baseForm('bookForm')(state, action);
