import React from 'react';
import FontIcon from 'react-toolbox/lib/font_icon';
import moment from 'moment';
import styles from './EventInfo.scss';

export default class EventInfo extends React.Component
{
    static propTypes = {
        event: React.PropTypes.object
    };

    render() {
console.log(this.props.event);
        const startDate = moment(this.props.event.startDate, "YYYY-MM-DD at HH-mm-ss");
        const endDate = moment(this.props.event.endDate, "YYYY-MM-DD at HH-mm-ss");

        return (
            <div className="event-info">
                <h1 className="event-info__name"> { this.props.event.name }</h1>


                <div className="event-info__content">

                    <div className="event-info__info">
                        <div className="event-info__info-icon">
                            <FontIcon value='place' />
                        </div>

                        <div className="event-info__info-content">
                            { this.props.event.location }
                        </div>
                    </div>

                    <div className="event-info__info">
                        <div className="event-info__info-icon">
                            <FontIcon value='today' />
                        </div>

                        <div className="event-info__info-content">
                            <div className="event-info__date">
                                {startDate.format('D MMM YY') + ' at '}
                                {startDate.format('HH:mm:ss')}
                            </div>
                            <div className="event-info__date">
                                {endDate.format('D MMM YY') + ' at '}
                                {endDate.format('HH:mm:ss')}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}