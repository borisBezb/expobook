import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BookForm from './BookForm';
import * as actions from '../actions/stand';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import EventInfo from '../components/EventInfo';

@connect(
    state => ({
        isFetching: state.stand.isFetching,
        stand: state.stand.detail
    }),
    dispatch => ({
        actions: bindActionCreators(actions, dispatch)
    })
)
export default class BookPage extends React.Component {

    componentWillMount() {
        const id = this.props.routeParams.id;
        this.props.actions.fetchStand(id);
    }

    render() {
        let content = null;

        if(false === this.props.isFetching) {
            const goBack = (e) => {
                e.preventDefault();

                this.props.router.push(`/event/${ this.props.stand.event.id }`)
            };

            content = (
                <div>
                    <a href="#" onClick={ (e) => goBack(e) }>back to event page</a>
                    <EventInfo event={ this.props.stand.event }/>
                    <BookForm stand={ this.props.stand }/>
                </div>);
        } else {
            content = (<ProgressBar type="circular" mode="indeterminate" />);
        }

        return (
            <div className="app-content">
                <div className="single-wrapper">
                    { content }
                </div>
            </div>
        );
    }
}
