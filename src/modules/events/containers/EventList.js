import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/events';
import Map from './Map';
import EventSidebar from './EventSidebar';
import friendlyRouter from 'modules/app/services/friendlyRouter';

@connect(
    state => ({
        selectedEvent: state.events.selectedEvent,
        isFetching: state.events.isFetching,
        events: state.events.items
    }),
    dispatch => ({
        actions: bindActionCreators(actions, dispatch)
    })
)
export default class EventList extends React.Component {
    @friendlyRouter('event_list')
    componentWillMount () {
        this.props.actions.fetchEvents();
    }

    componentWillUnmount() {
        this.props.actions.unselectEvent();
    }

    render() {
        const toggleState = () => {
            if(!this.props.selectedEvent) {
                return 'app-content';
            }

            return 'app-content app-content_expanded';
        };

        const getSelectedEvent = () => {
            if(!this.props.selectedEvent) {
                return '';
            }

            let filtered = this.props.events.filter((event) => event.id == this.props.selectedEvent);
            if(0 == filtered.length) {
                return '';
            }

            return (<EventSidebar event={ filtered[0] }/>);
        };

        return (
            <div className={ toggleState() }>
                <Map events = { this.props.events } isFetching={ this.props.isFetching }/>
                <div className="sidebar"> { getSelectedEvent() }</div>
            </div>
        );
    }
}