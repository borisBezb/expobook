import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/event';
import styles from './EventDetail.scss';
import Stand from './Stand';
import EventInfo from '../components/EventInfo';
import friendlyRouter from 'modules/app/services/friendlyRouter';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import Dialog from 'react-toolbox/lib/dialog';

@connect(
    state => ({
        isFetching: state.event.isFetching,
        event: state.event.detail
    }),
    dispatch => ({
        actions: bindActionCreators(actions, dispatch)
    })
)
export default class EventDetail extends React.Component {

    state = {
        stand: {
            company: {}
        },
        dialogOpened: false
    };
    actions = [
        { label: "Reservate", onClick: this.goToStand }
    ];

    @friendlyRouter('event_detail')
    componentWillMount() {
        const id = this.props.routeParams.id;
        this.props.actions.fetchEvent(id);
    }

    goToStand() {
        this.props.router.push(`/stand/${ this.state.stand.id }/book`)
    }

    clickStandInfo = (stand) => {
        this.setState({
            stand: stand,
            dialogOpened: true
        })
    };

    handleToggle = () => {
        this.setState({dialogOpened: !this.state.dialogOpened});
    }

    render() {
        let stands = '';
        if(Object.keys(this.props.event).length) {
            stands = this.props.event.stands.map((stand, i) => {
                return (
                    <Stand stand={ stand } eventId={ this.props.event.id }  onClick={ this.clickStandInfo } key={i}/>
                )
            });
        }

        let content = null;
        if(true === this.props.isFetching) {
            content = (
                <div className="single-wrapper">
                    <ProgressBar type="circular" mode="indeterminate" />
                </div>);
        } else {
            content = (
                <div className="single-wrapper">
                    <EventInfo event={ this.props.event }/>

                    <div className="hall-container">
                        <h2 className="hall-container__title">Hall map:</h2>
                        <div className="hall-map">
                            { stands }
                        </div>
                    </div>

                    <Dialog
                        actions={this.actions}
                        active={this.state.dialogOpened}
                        onEscKeyDown={this.handleToggle}
                        onOverlayClick={this.handleToggle}
                        title={ this.state.stand.company ? this.state.stand.company.name : null }
                    >
                        <p>{ this.state.stand.company ? this.state.stand.company.contacts : null }</p>
                    </Dialog>
                </div>);
        }


        return (
            <div className="app-content">
                { content }
            </div>
        );
    }
}