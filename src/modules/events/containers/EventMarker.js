import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/events';
import { Marker } from 'components/icons';

@connect(
    (state, props) => ({
        selected: state.events.selectedEvent == props.event.id
    }),
    dispatch => ({
        actions: bindActionCreators(actions, dispatch)
    })
)
export default class EventMarker extends React.Component {
    static propTypes = {
        event: React.PropTypes.object.isRequired,
        selected: React.PropTypes.bool.isRequired,
        maps: React.PropTypes.object.isRequired,
        mapOverlay: React.PropTypes.object.isRequired
    };

    static defaultProps = {
        mounted: false,
        position: {}
    };

    componentDidMount() {
        this.props.mapOverlay.addMapListener('zoom_changed', () => {
            this.forceUpdate();
        });
    }

    onClick = () => {
        if(false === this.props.selected) {
            this.props.actions.selectEvent(this.props.event.id);
        } else {
            this.props.actions.unselectEvent();
        }
    };

    render() {
        const markerClass = () => {
            return 'marker' + (this.props.selected == true ? ' marker_selected' : '');
        };

        const latLng = new this.props.maps.LatLng(this.props.event.latitude, this.props.event.longitude);
        const point = this.props.mapOverlay.getProjection().fromLatLngToDivPixel(latLng);

        return (
            <div
                className={ markerClass() }
                onClick={ this.onClick }
                style={{
                    left: point.x + 'px',
                    top: point.y + 'px'
                }}
                ref="marker"
            >
                <Marker/>
            </div>
        );
    }
}