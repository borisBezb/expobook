import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './Stand.scss';
import Tooltip from 'react-toolbox/lib/tooltip';
import { push } from 'react-router-redux';

@connect()
export default class Stand extends React.Component {

    static propTypes = {
        stand: React.PropTypes.object.isRequired,
        eventId: React.PropTypes.number.isRequired,
        onClick: React.PropTypes.func
    };

    goToBook = (e) => {
        e.preventDefault();
        this.props.dispatch(push(`/stand/${ this.props.stand.id }/book`))
    };

    bookedView(positionStyles) {
        return (
            <div className="stand stand_disabled" style={ positionStyles } onClick={ () => this.props.onClick(this.props.stand) }>
                <div className="stand__company">
                    <img className="stand__company-logo" src={ this.props.stand.company.logo }/>

                    <span className="stand__company-name">
                        { this.props.stand.company.name }
                    </span>
                </div>

            </div>
        )
    }

    freeView(positionStyles) {
        return (
            <div className="stand" style={ positionStyles }>
                <div className="stand__price">
                    { this.props.stand.price + ' ' + this.props.stand.currency }
                </div>
                <a href="#" onClick={ (e) => this.goToBook(e)} className="stand__button" style={{lineHeight: this.props.stand.height + 'px'}}>Book</a>
            </div>
        )
    }

    render() {
        const positionStyles = {
            width: this.props.stand.width + 'px',
            height: this.props.stand.height + 'px',
            left: this.props.stand.offsetX + 'px',
            top: this.props.stand.offsetY + 'px'
        };

        if(this.props.stand.company) {
            return this.bookedView(positionStyles);
        } else {
            return this.freeView(positionStyles);
        }
    }
}