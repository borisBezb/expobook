import React from 'react';
import { connect } from 'react-redux';
import Button from 'react-toolbox/lib/button';
import { bindActionCreators } from 'redux';
import BaseForm from 'modules/forms/BaseForm';
import * as actions from '../actions/stand';
import {setSnackbarMessage} from 'modules/app/actions'
import { push } from 'react-router-redux';

@connect(
    state => ({}),
    dispatch => ({
        book: (stand, data, onErrors) => dispatch(actions.fetchBooking(stand, data, onErrors)),
        dispatch
    })
)
@BaseForm({
    validationRules: {
        email: ['required', 'email'],
        name: ['required'],
        admin: ['required'],
        contacts: ['required'],
        logo: ['required']
    },
    formState: 'bookForm'
})
class BookForm extends React.Component {
    static propTypes = {
        stand: React.PropTypes.object
    };

    componentDidMount() {

    }

    submitHandler = (formProps, onErrors) => {
        this.props.book(this.props.stand, formProps.model, onErrors);
    };

    render() {
        return (
            <form onSubmit={ (e) => this.props.onSubmit(e, this.submitHandler) }>
                <h1>Place reservation</h1>
                <div className="block input-list">
                    {
                        this.props.renderInput({
                            name: 'name',
                            label: "Company name",
                            placeholder: 'Type company name'
                        })
                    }

                    {
                        this.props.renderInput({
                            name: 'admin',
                            label: 'Contact person',
                            placeholder: 'Select contact person'
                        })
                    }

                    {
                        this.props.renderInput({
                            name: 'email',
                            type: 'email',
                            label: 'Email',
                            placeholder: 'Type contact email'
                        })
                    }

                    {
                        this.props.renderInput({
                            name: 'contacts',
                            label: 'Contacts',
                            placeholder: 'Please fill company contact information'
                        })
                    }

                    {
                        this.props.renderFileInput({
                            name: 'logo',
                            label: 'Company logo:',
                            type: 'image',
                            ref: (name) => this.refs[name]
                        })
                    }

                    <div className="form-field">
                        <Button label='Reservate the place' type="submit" raised primary />
                    </div>
                </div>
            </form>
        );
    }
}

export default BookForm;
