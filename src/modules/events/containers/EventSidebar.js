import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Button from 'react-toolbox/lib/button';
import EventInfo from '../components/EventInfo';
import styles from './EventSidebar.scss';

@connect()
export default class EventSidebar extends React.Component
{
    static propTypes = {
        event: React.PropTypes.object
    };

    render() {
        const goToEvent = () => {
            this.props.dispatch(push(`/event/${ this.props.event.id }`))
        };

        return (
            <div className="event-sidebar">
                <EventInfo event={ this.props.event }/>
                <div className="event-info__button-place">
                    <Button label='Book your place' onClick={ goToEvent } raised primary />
                </div>
            </div>
        );
    }
}