import React from 'react';
import MapLoader from '../map/MapLoader';

const EIFFEL_TOWER_POSITION = {
    lat: 55.01227627898973,
    lng: 82.92789459228516
};

export default class Map extends React.Component {
    static propTypes = {
        isFetching: React.PropTypes.bool.isRequired,
        events: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.isInit = new Promise(resolve => {
            MapLoader.then((mapClasses) => {
                this.map = new mapClasses.maps.Map(this.refs.map, {
                    center: EIFFEL_TOWER_POSITION,
                    zoom: 13
                });

                this.map.addListener('click', (event) => {
                    console.log(event.latLng.lat() + ',' +  event.latLng.lng());
                });

                this.markerOverlay = new mapClasses.MarkerOverlay(this.map);
                resolve();
            });
        });
    }

    componentWillUnmount() {
        this.markerOverlay.onRemove();
    }

    componentDidUpdate(prevProps) {
        if(true === prevProps.isFetching && this.props.isFetching != prevProps.isFetching) {
            this.isInit.then(() => this.drawMarkers());
        }
    }

    drawMarkers() {
        this.props.events.forEach((item) => this.markerOverlay.addMarker(item));
        this.markerOverlay.draw()
    }

    render() {
        return (
            <div ref="map" className="map"></div>
        );
    }
}