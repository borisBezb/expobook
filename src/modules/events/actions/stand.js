import { stands } from '../resources';
import { push } from 'react-router-redux';
import {setAppMessage} from 'modules/app/actions';

export const REQUEST_STAND = 'REQUEST_STAND';
export const FETCH_STAND = 'FETCH_STAND';
export const FETCH_BOOKING = 'FETCH_BOOKING';
export const REQUEST_BOOKING = 'REQUEST_BOOKING';

export const fetchStand = (id) => dispatch => {
    dispatch(requestStand());

    return stands.get(id).then((response) => {
        dispatch({
            type: FETCH_STAND,
            detail: response.data.response
        });
    })
};

export const requestStand = () => ({
    type: REQUEST_STAND
});

export const fetchBooking = (stand, data, onErrors) => dispatch => {
    dispatch(requestBooking());

    return stands.book(stand.id, data)
        .then(response => {
            const data = response.data.response.response;
            dispatch({
                type: FETCH_BOOKING
            });

            dispatch(setAppMessage('Place has been booked'));
            dispatch(push(`/event/${ stand.event.id }`))
        })
        .catch(error => {
            const data = error.response.data;
            dispatch({
                type: FETCH_BOOKING
            });

            if(data.code == 400 && data.params.code == 1) {
                dispatch(setAppMessage(data.message));
                dispatch(push(`/event/${ stand.event.id }`))
            } else {
                onErrors(error.response);
            }
        });
};

export const requestBooking = () => ({
    type: REQUEST_BOOKING
});
