import { events } from '../resources';

export const FETCH_EVENT = 'FETCH_EVENT';
export const REQUEST_EVENT = 'REQUEST_EVENTS';

export const fetchEvent = (id) => dispatch => {
    dispatch(requestEvent());

    return events.get(id).then((response) => {
        dispatch({
            type: FETCH_EVENT,
            detail: response.data.response
        });
    })
};

export const requestEvent = () => ({
    type: REQUEST_EVENT
});