import { events } from '../resources';

export const FETCH_EVENTS = 'FETCH_EVENTS';
export const REQUEST_EVENTS = 'REQUEST_EVENTS';
export const SELECT_EVENT = 'SELECT_EVENT';
export const UNSELECT_EVENT = 'UNSELECT_EVENT';

export const requestEvents = () => ({
    type: REQUEST_EVENTS
});

export const fetchEvents = () => dispatch => {
    dispatch(requestEvents());

    return events.getList().then((response) => {

        dispatch({
            type: FETCH_EVENTS,
            items: response.data.response
        });
    })
};

export const selectEvent = (id) => ({
    type: SELECT_EVENT,
    id
});

export const unselectEvent = () => ({
    type: UNSELECT_EVENT
});