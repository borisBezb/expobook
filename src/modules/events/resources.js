import Api from 'services/Api';

export const events = {
    prefix: '/event',

    getList: function() {
        return Api.get(this.prefix);
    },

    get: function(id) {
        return Api.get(this.prefix + '/' + id);
    },

    getStands: function (id) {
        return Api.get(this.prefix + '/' + id + '/stands');
    }
};

export const stands = {
    prefix: '/stand',

    book: function(id, data) {
        return Api.post(this.prefix + '/' + id + '/book', data);
    },

    get: function(id) {
        return Api.get(this.prefix + '/' + id);
    }
};