import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as actions from './actions';
import * as validators from './validators';
import fileResource from './resource';
import Input from 'react-toolbox/lib/input';
import Button from 'react-toolbox/lib/button';
import FontIcon from 'react-toolbox/lib/font_icon';
import styles from './styles.scss';

const BaseForm = options => {
    const formName = options.formState;
    return WrappedComponent => {

        class Form extends React.Component {
            static propTypes = {
                formProps: React.PropTypes.shape({
                    model: React.PropTypes.object.isRequired,
                    errors: React.PropTypes.object.isRequired,
                    submitted: React.PropTypes.bool.isRequired
                })
            };

            constructor(props) {
                super(props);
                this.validationRules = options.validationRules;
                let modelKeys = Object.keys(this.props.formProps.model);

                this.changeList = modelKeys.reduce((memo, key) => {
                    memo[key] = false;
                    return memo;
                }, {});

                this.asyncErrors = modelKeys.reduce((memo, key) => {
                    memo[key] = [];
                    return memo;
                }, {});
            }

            componentWillUnmount() {
                this.props.actions.reset(formName);
            }

            getFieldValue = (field) => this.props.formProps.model[field];
            getFieldErrors = (field) => this.props.formProps.errors[field];

            onSubmit = (event, handler) => {
                event.preventDefault();

                this.props.actions
                    .submit(formName)
                    .then(() => {
                        if(this.isValid() === true) {
                            handler(this.props.formProps, this.onServerErrors);
                        }
                    });
            };

            onServerErrors = (response) => {
                // Если получаем ошибку формы, то выводим полученные ошибки
                if (response.status == 400) {
                    this.parseAsyncErrors(response.data.children);
                }
            };

            parseAsyncErrors = (data) => {
                if (data == undefined) {
                    return;
                }

                let isFound = false;
                Object.keys(data)
                    .forEach((key) => {
                    console.log(data[key].errors);
                        if(data[key].errors == undefined) {
                            return;
                        }

                        data[key].errors.forEach(
                            (error) => {
                                isFound = true;
                                this.asyncErrors[key].push(error);
                            }
                        )

                    });

                if(isFound == true) {
                    this.forceUpdate()
                }
            };

            onBlur = (field) => {
                let isChanged = this.changeList[field];
                this.changeList[field] = false;

                if(isChanged == true) {
                    this.validateField(field);

                    if(this.asyncErrors[field].length > 0) {
                        this.asyncErrors[field] = [];
                    }
                }
            };

            isValid() {
                let isValid = true;
                Object.keys(this.validationRules).forEach(
                    (field) => {
                        let fieldErrors = this.validateField(field);

                        if(fieldErrors.length > 0) {
                            isValid = false;
                        }
                    }
                );

                return isValid;
            }

            updateField = (field, value) => {
                this.props.actions.update(formName, field, value);
                this.changeList[field] = true;

                /**
                 * Поля без ошибок на ходу не валидируем, только при потери фокуса.
                 * Если поле уже с ошибкой, то валидируем каждое изменение, чтобы
                 * вовремя сигнализировать пользователю, что ошибка исправлена
                 */
                if(this.getFieldErrors(field).length > 0) {
                    this.validateField(field);
                }
            };

            validateField = (field) => {
                const validationRules = this.validationRules[field];
                const fieldValue = this.getFieldValue(field);

                let errors = validationRules
                    .reduce((memo, validator) => {
                        if(typeof validator == 'string') {
                            if(!validators[validator]) {
                                return memo;
                            }

                            return memo.concat(
                                validators[validator](fieldValue, this.props.formProps)
                            );
                        }

                        if(validator instanceof Object) {
                            let validatorName = Object.keys(validator)[0];

                            if(!validators[validatorName]) {
                                return memo;
                            }

                            return memo.concat(
                                validators[validatorName](
                                    fieldValue,
                                    this.props.formProps,
                                    validator[validatorName]
                                )
                            );
                        }

                        return memo;
                    }, []);

                this.props.actions.setFieldErrors(formName, field, errors);

                return errors;
            };

            renderInput = (field) => {
                const errors = this.renderFieldErrors(field.name);
                return (
                    <Input
                        type = { field.type }
                        name = { field.name }
                        value = { this.props.formProps.model[field.name] }
                        label = { field.label }
                        hint = { field.placeholder }
                        onChange = { this.updateField.bind(this, field.name) }
                        onBlur = { () => this.onBlur(field.name) }
                        error = {
                            errors.length ? (
                                <div>
                                    {errors}
                                </div>
                            ) : null
                        }
                    />
                );
            };

            renderFileInput = (field) => {
                const choiceFile = () => {
                    field.ref(field.name).click();
                };

                const startUpload = () => {
                    const fieldInput = field.ref(field.name);
                    const formData = new FormData();
                    formData.append('file', fieldInput.files[0], fieldInput.files[0].name);

                    fileResource
                        .upload(field.type, formData)
                        .then(response => {
                            const fileId = response.data.response.id;
                            this.updateField(field.name, fileId);
                        })
                        .catch(error => {
                            const response = error.response;
                            const errors = [];

                            if(response.status == 400) {
                                try {
                                    response.data.children.file.errors.forEach((error) => {
                                        errors.push(error);
                                    });
                                } catch (e) {
                                    errors.push('Unknown error, please try again later');
                                }

                                if(errors.length) {
                                    this.props.actions.setFieldErrors(formName, field.name, errors);
                                }
                            }

                            if(response.status == 413) {
                                errors.push('file is too large');
                                this.props.actions.setFieldErrors(formName, field.name, errors);
                            }

                            console.log(error.response)
                        });
                };

                const errors = this.renderFieldErrors(field.name);

                return (
                    <div className="form-file-field">
                        <div className="form-file-field__data">
                            <label className="form-file-field__label">
                                { field.label }
                                <input
                                    type="file"
                                    name={ field.name }
                                    className="form-file-field__input"
                                    ref={ field.name }
                                    onChange={ startUpload }
                                />
                            </label>

                            <div className="form-file-field__button">
                                <Button label='upload' type="button" onClick={ choiceFile } raised />
                            </div>
                            {
                                this.getFieldValue(field.name) ? (
                                    <div className="form-file-field__result">
                                        <FontIcon value="done" /> File has been uploaded
                                    </div>
                                ) : null
                            }

                        </div>
                        {
                            errors.length ? (
                                <div className="form-file-field__errors">
                                    {errors}
                                </div>
                            ) : null
                        }
                    </div>
                );
            };

            renderFieldErrors(field) {
                let errors = [];
                this.getFieldErrors(field).forEach(
                    (error, i) => {
                        errors.push(<div key={i}>{error}</div>);
                    }
                );

                //console.log(this.props)
                if(this.asyncErrors != 'undefined' && this.asyncErrors[field] != 'undefined') {
                    this.asyncErrors[field].forEach(
                        (error, i) => {
                            errors.push(<div key={i}>{error}</div>);
                        }
                    );
                }

                return errors;
            }

            render() {
                const {
                    actions,
                    formProps,
                    dispatch,
                    ...rest
                } = this.props;
                const propsToPass = {
                    renderInput: this.renderInput,
                    renderFileInput: this.renderFileInput,
                    validateField: this.validateField,
                    updatedField: this.updateField,
                    isValid: this.isValid,
                    onSubmit: this.onSubmit,
                    ...rest
                };

                return React.createElement(WrappedComponent, propsToPass);
            }
        }

        const connector = connect(
            state => ({
                formProps: state[options.formState]
            }),
            dispatch => ({
                actions: bindActionCreators(actions, dispatch),
                dispatch
            })
        );

        return connector(Form);
    }
};

export default BaseForm;
