import Api from 'services/Api';

export default {
    prefix: '/file',

    upload: function(type, formData) {
        return Api.post(this.prefix + '/' + type, formData);
    }
}