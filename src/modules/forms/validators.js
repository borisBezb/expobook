export function required(value, formProps) {

    if(!value && formProps.submitted == true) {
        return ['It is required field'];
    }

    return [];
}

export function email(value) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!value) {
        return [];
    }

    return !regex.test(value) ? ['Incorrect email'] : [];
}

export function twoFieldsEqual(value, formProps, options) {
    let model = formProps.model;

    if(!value || model[options.field] === undefined) {
        return [];
    }

    return value != model[options.field] ? ['Passwords do not match'] : [];
}