export const UPDATE_FORM_VALUE = 'UPDATE_FORM_VALUE';
export const RESET_FORM = 'RESET_FORM';
export const SUBMIT_FORM = 'SUBMIT_FORM';
export const SET_FIELD_ERRORS = 'SET_FIELD_ERRORS';

export const formActionType = (formName, type) => formName + '.' + type;

export function update(formName, name, value) {
    return {
        type: formActionType(formName, UPDATE_FORM_VALUE),
        name, value
    }
}

export function submit(formName) {
    return dispatch => {
        dispatch({
            type: formActionType(formName, SUBMIT_FORM)
        });

        return Promise.resolve();
    }
}

export function reset(formName) {
    return {
        type: formActionType(formName, RESET_FORM)
    }
}

export function setFieldErrors(formName, field, errors) {
    return {
        type: formActionType(formName, SET_FIELD_ERRORS),
        field,
        errors
    }
}