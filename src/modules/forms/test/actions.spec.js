import * as actions from '../actions'

import initialState from 'initialState';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('form actions', () => {

    it('should update field value', () => {
        const expectedAction = {
            type: 'form.' + actions.UPDATE_FORM_VALUE,
            name: 'login',
            value: 'boris'
        };

        expect(actions.update('form', 'login', 'boris')).toEqual(expectedAction)
    });

    it('should set submitted value is true', () => {
        const expectedActions = [
            { type: 'form.' + actions.SUBMIT_FORM }
        ];

        const store = mockStore({submitted: false});
        return store.dispatch(actions.submit('form'))
            .then(() => { // return of async actions
                expect(store.getActions()).toEqual(expectedActions)
            })
    });

    it('should define that form has been reset', () => {
        const expectedAction = {
            type: 'form.' + actions.RESET_FORM
        };

        expect(actions.reset('form')).toEqual(expectedAction)
    });


    it('should set field errors', () => {
        const expectedAction = {
            type: 'form.' + actions.SET_FIELD_ERRORS,
            field: 'login',
            errors: []
        };

        expect(actions.setFieldErrors('form', 'login', [])).toEqual(expectedAction)
    });
})