import reducer from '../reducer'
import * as actions from '../actions'

describe('form reducer', () => {
    it('should return the initial state', () => {
        expect(
            reducer('form')(undefined, {})
        ).toEqual({
            submitted: false,
            model: {},
            errors: {}
        })
    })

    it('should handle UPDATE_FORM_VALUE', () => {
        expect(
            reducer('form')({
                submitted: false,
                model: {
                    field: ''
                },
                errors: {
                    field: []
                }
            }, {
                type: actions.formActionType('form', actions.UPDATE_FORM_VALUE),
                name: 'field',
                value: 'a'
            })
        ).toEqual(
            {
                submitted: false,
                model: {
                    field: 'a'
                },
                errors: {
                    field: []
                }
            }
        )
    })

    it('should handle SUBMIT_FORM', () => {
        expect(
            reducer('form')({
                submitted: false,
                model: {
                    field: ''
                },
                errors: {
                    field: []
                }
            }, { type: actions.formActionType('form', actions.SUBMIT_FORM)})
        ).toEqual(
            {
                submitted: true,
                model: {
                    field: ''
                },
                errors: {
                    field: []
                }
            }
        )
    })

    it('should handle RESET_FORM', () => {
        expect(
            reducer('form')({
                submitted: true,
                model: {
                    field: 'a'
                },
                errors: {
                    field: []
                }
            }, { type: actions.formActionType('form', actions.RESET_FORM)})
        ).toEqual(
            {
                submitted: false,
                model: {
                    field: ''
                },
                errors: {
                    field: []
                }
            }
        )
    })


    it('should handle SET_FIELD_ERRORS', () => {
        expect(
            reducer('form')({
                submitted: true,
                model: {
                    field: 'a'
                },
                errors: {
                    field: []
                }
            }, {
                type: actions.formActionType('form', actions.SET_FIELD_ERRORS),
                field: 'field',
                errors: ['There is required field']
            })
        ).toEqual(
            {
                submitted: true,
                model: {
                    field: 'a'
                },
                errors: {
                    field: ['There is required field']
                }
            }
        )
    })
})