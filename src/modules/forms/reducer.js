import * as actions from './actions';

const initialState = {
    submitted: false,
    model: {},
    errors: {}
};

export default (formName) => (state = initialState, action) => {
    switch(action.type) {
        case actions.formActionType(formName, actions.UPDATE_FORM_VALUE):
        {
            if(state.model[action.name] === undefined) {
                return state;
            }

            let newState = Object.assign({}, state);
            newState.model[action.name] = action.value;

            return newState;
        }

        case actions.formActionType(formName, actions.SUBMIT_FORM):
            return Object.assign({}, state, {submitted: true});

        case actions.formActionType(formName, actions.SET_FIELD_ERRORS):
        {
            if(state.errors[action.field] === undefined) {
                return state;
            }

            let newState = Object.assign({}, state);
            newState.errors[action.field] = action.errors;

            return newState;
        }

        case actions.formActionType(formName, actions.RESET_FORM):
            let newState = Object.assign({}, state, {submitted: false});

            Object.keys(newState.model).forEach((key) => {
                newState.model[key] = '';
            });

            Object.keys(newState.errors).forEach((key) => {
                newState.errors[key] = [];
            });

            return newState;

        default:
            return state;
    }
};