import initialState from 'initialState';
import * as actions from './actions';

export default (state = initialState.app, action) => {
    switch(action.type) {
        case actions.APP_SET_MESSAGE:
            return {...state, message: action.message};

        case actions.APP_SET_STATE:
            return {...state, state: action.state};

        default:
            return state;
    }
};