import store from 'store';
import { setAppState } from '../actions';

export default (state) => (target, name, descriptor) => {
    let originalFn = descriptor.value;

    console.log(descriptor);
    let newFn = function() {
        console.log('update!');
        store.dispatch(setAppState(state));
        console.log(target);
        originalFn.apply(this, arguments);
    };

    descriptor.value = newFn;
    return descriptor;
};