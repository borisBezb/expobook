export const APP_SET_MESSAGE = 'APP_SET_MESSAGE';
export const APP_SET_STATE = 'APP_SET_STATE';

export const setAppMessage = (message) => {
    return {
        type: APP_SET_MESSAGE,
        message
    }
};

export const setAppState = (state) => {
    return {
        type: APP_SET_STATE,
        state
    }
};