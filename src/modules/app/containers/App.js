import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './App.scss';
import DevTools from 'containers/DevTools';
import Snackbar from 'react-toolbox/lib/snackbar';
import {setAppMessage} from '../actions';

@connect(
    state => ({
        app: state.app
    })
)
export default class App extends React.Component {

    render() {

        let headerClass = 'header__wrapper wrapper';

        if(this.props.app.state != 'event_list') {
            headerClass += ' header__wrapper_short'
        }

        const handleMessageTimeout = () => {
            this.props.dispatch(setAppMessage(''));
        };

        const isMessage = this.props.app.message != '';

        const goTo = (e, url) => {
            e.preventDefault();
            this.props.router.push(url);
        };

        return (
            <div>
                <header className="header">
                    <div className={ headerClass }>
                        <div className="header__logo">
                            <a href="#" onClick={ (e) => { goTo(e, `/`) }}>Expobooks</a>
                        </div>

                        <ul className="header-menu">
                            {
                                this.props.app.state != 'event_list' ? (
                                    <li><a href="#" onClick={ (e) => { goTo(e, `/`) }}>back to map</a></li>
                                ) : null
                            }
                        </ul>
                    </div>
                </header>

                { this.props.children }

                <Snackbar
                    action='Dismiss'
                    active={ isMessage }
                    label={ this.props.app.message }
                    timeout={ 3000 }
                    onTimeout={ handleMessageTimeout }
                    type='cancel'
                />


            </div>
        );
    }
};
