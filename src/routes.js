import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './modules/app/containers/App';
import EventList from './modules/events/containers/EventList';
import EventDetail from './modules/events/containers/EventDetail';
import BookPage from './modules/events/containers/BookPage';

const onEnter = (params, replace) => {
    console.log(params)
}

export default (
    <Route path="/" component={ App }>
        <IndexRoute component={ EventList } />

        <Route path="/event/:id" component={ EventDetail } onEnter={onEnter}/>
        <Route path="/stand/:id/book" component={ BookPage }/>
    </Route>
);
